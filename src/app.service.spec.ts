import { AppService } from './app.service';
import { Test } from '@nestjs/testing';
import { AppController } from './app.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { userSchema } from './user.model';
import  * as mongoose from 'mongoose';




describe('AppService', () => {
  let appService :AppService;
  beforeEach(async()=>{
    const module =await Test.createTestingModule({
      imports:[MongooseModule.forRoot('mongodb://localhost:27017/test-minicrud'),MongooseModule.forFeature([{ name: 'User', schema: userSchema }])],
      controllers:[AppController],
      providers:[AppService]
    }).compile();
    appService = module.get<AppService>(AppService);
  })

  describe('Create', () => {
  beforeEach(async()=>{
  await appService.deleteAll()
  })
    it('should define service',()=>{
      expect(appService).toBeDefined()
    })
    let userParams  = {  name: 'firstName', email: 'test@email.com', password: '123' };

    it("should create a user", async() => {
      const createdUser =await appService.create(userParams);
      expect(createdUser).toHaveProperty('id');
    })
    it("should return user already exist", async() => {
     await appService.create(userParams);
      const createdUser =await appService.create(userParams);
      expect(createdUser).toEqual("User already exist")
    })
   })

  describe('getAll', () => {
    let users;
    let createdUsers;
    beforeEach(async() => {
  await appService.deleteAll()

      users = [
        { name: "Shweta", email: "bshweta000@gmail.com", password: "123" },
        { name: "Rahul", email: "rahul000@gmail.com", password: "123" },
        { name: "Varun", email: "varun000@gmail.com", password: "123" }
      ];
    createdUsers= await Promise.all( users.map(async user =>await appService.create(user)))
    })
    it("should return all users", async() => {
      expect(await appService.getAll()).toHaveLength(3);
    })
   
    it("should return true value if user deleted", async() => {
      const deletedData = await appService.delete(createdUsers[1].id);
      expect(deletedData).toBe(true)
    })

    it("should return user if id was passed in params", async() => {
      const existUser = await appService.getById(createdUsers[2].id);
      expect(existUser.toJSON()).toEqual(createdUsers[2].toJSON())
    })

    it("should return user not found", async () => {
      await appService.delete(createdUsers[1].id)
      const existingUser = await appService.getById(createdUsers[1].id)
      expect(existingUser).toEqual("user not exist")
    })

  //   it("should return the updated value",async () => {
      
  //     const updatedUser = await appService.update({...users[0],name:'Shweta1',password:'1234'}, createdUsers[0].id);
  //     expect(updatedUser).toEqual( {
      
  //       "__v": 0,
  //       "_id": mongoose.Types.ObjectId(createdUsers[0].id),
  //       "email": "bshweta000@gmail.com",
  //       "name": "Shweta1",
  //       "password": "1234"
  //     })
  //    })
  })

  afterEach(() => {
    jest.resetAllMocks();
  })
})



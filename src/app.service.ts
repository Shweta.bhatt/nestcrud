import { Injectable } from '@nestjs/common';
import { User, CreateUserParams } from './interfaces/app.interfaces';
import {Model} from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class AppService {
  constructor(@InjectModel('User') private readonly userModel: Model<any>) {}


  private users: User[] = [];
  private count = 0;

 async create(user: CreateUserParams):Promise<any> {
    const existingUser =await this.userModel.findOne({email:user.email});
    if(existingUser) return 'User already exist';
    return this.userModel.create(user);
  }


  getAll(): Promise<User[]> {
    return this.userModel.find({});
  }

  async update(updatedAtrr : User,id:String):Promise<User>
  {
   return await this.userModel.findByIdAndUpdate(id,updatedAtrr,{new: true})

  }

 
async delete(id){
 return !!await this.userModel.findByIdAndDelete(id)
  }

  deleteAll(){
    return this.userModel.remove({})
  }

  async getById(id){
    const userById= await this.userModel.findById(id)
    if(!userById)
    return "user not exist"
    else
    return userById
  }
}

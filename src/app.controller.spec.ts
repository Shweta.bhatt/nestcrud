import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { userSchema } from './user.model'
import * as mongoose from 'mongoose';


describe('AppController', () => {
  let appController: AppController;
  let appService: AppService;
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [MongooseModule.forRoot('mongodb://localhost:27017/test-minicrud'), MongooseModule.forFeature([{ name: 'User', schema: userSchema }])],
      controllers: [AppController],
      providers: [AppService]
    }).compile();
    appService = module.get<AppService>(AppService);
    appController = module.get<AppController>(AppController);
  })

  describe('create', () => {
    beforeEach(async () => {
      await appService.deleteAll()
    })
    const users = [
      { id: 0, name: "Shweta", email: "bshweta000@gmail.com", password: "123" },
      { id: 1, name: "Rahul", email: "rahul000@gmail.com", password: "123" },
      { id: 2, name: "Varun", email: "varun000@gmail.com", password: "123" }
    ];

    it("should return created user", async () => {
      jest.spyOn(appService, 'create').mockResolvedValue(users)
      expect(await appController.createUsers(users)).toBe(users)
    })
    it("should return user already exist", async () => {
      jest.spyOn(appService, 'create').mockResolvedValue("email already exist")
      expect(await appController.createUsers(users[0])).toEqual("email already exist")
    })
    it("should return all user details", async () => {
      jest.spyOn(appService, 'getAll').mockResolvedValue(users)
      expect(await appController.getUsers()).toBe(users);
    })
    it("should return true if user deleted", async () => {
      jest.spyOn(appService, 'delete').mockResolvedValue(true)
      expect(await appController.deletedUser(users[0].id)).toEqual({"success":true})
    })
    it("should return user if id passed through params", async () => {
      jest.spyOn(appService, 'getById').mockResolvedValue(users[1])
      expect(await appController.getUser(users[1].id)).toBe(users[1])
    })
    it("should return user not found", async () => {
      jest.spyOn(appService, 'getById').mockResolvedValue("User not found")
      expect(await appController.getUser(users[0].id)).toBe("User not found")
    })
    // it("should return the updated user details", () => {
    //         jest.spyOn(appService, 'update').mockImplementation(() => (users[0] = {
    //           id: 0,
    //           name: "Shweta1",
    //           email: "bshweta000@gmail.com",
    //           password: "1232"
    //         }))
    //         expect(appController.updateUsers({
    //           id: 0,
    //           name: "Shweta1",
    //           email: "bshweta000@gmail.com",
    //           password: "1232"
    //         })).toEqual(users[0])
    //       })
  })
})

//   describe('User Updation', () => {
//     it("should return the updated user details", () => {
//       jest.spyOn(appService, 'update').mockImplementation(() => (users[0] = {
//         id: 0,
//         name: "Shweta1",
//         email: "bshweta000@gmail.com",
//         password: "1232"
//       }))
//       expect(appController.updateUsers({
//         id: 0,
//         name: "Shweta1",
//         email: "bshweta000@gmail.com",
//         password: "1232"
//       })).toEqual(users[0])
//     })
//   })
//   afterEach(() => {
//     jest.resetAllMocks();
//   })
// })

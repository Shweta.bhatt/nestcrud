import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { AppModule } from './app.module';
import { AppService } from './app.service';
import { INestApplication } from '@nestjs/common';


describe('e2e', () => {
    let app: INestApplication;
    let users = [{id : 0, name : "Shweta", email : "bshweta000@gmail.com", password : "123"},
    {id : 1, name : "tanya", email : "tanya000@gmail.com", password : "123"},
    {id : 2, name : "seema", email : "seema000@gmail.com", password : "123"}]
    let appService = {
        getAll: () =>  users,
        create: () => users[0],
        delete: () => true
    }

    beforeAll(async () => {
        const module = await Test.createTestingModule({
            imports:[AppModule]
        })
          .overrideProvider(AppService)
          .useValue(appService)
          .compile();
    
        app = module.createNestApplication();
        await app.init();
      });

      it('user creation', async()=>{
        const response = await request(app.getHttpServer())
        .post('/users').send(users[0]);
        expect(response.status).toEqual(201);
        expect(response.body).toEqual(appService.create())
      })
      it('user deletion', async ()=>{
          const response = await request(app.getHttpServer())
          .delete(`/users/${users[1].id}`)
          expect(response.status).toEqual(200);
          expect(response.body).toEqual({success:appService.delete()})
      })

    it(`getting all users`, async() => {
    const response = await request(app.getHttpServer())
      .get('/users');
      expect(response.status).toEqual(200);
      expect(response.body).toEqual(appService.getAll())
  });

  afterAll(async () => {
    await app.close();
  });
    
})

  

// });
import { Controller, Get, Query, Post, Body, Put, Param, Delete, Req } from '@nestjs/common';
import { AppService } from './app.service';
import { User } from './interfaces/app.interfaces';

@Controller('users')
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Post()
  async createUsers(@Body() user: any) {
    const createdUser = await this.appService.create(user);
    return createdUser;
  }


  @Get()
  async getUsers(): Promise<User[]> {
    const users = await this.appService.getAll();
    return users;
  }

  @Put(':id')
  async updateUsers(@Body() user: any, @Param('id') id: String) {
    const updatedUser = await this.appService.update(user, id);
    return updatedUser;

  }




  @Delete(':id')
  async deletedUser(@Param('id') id: number): Promise<any> {
    const result = await this.appService.delete(id);
    return {success:result};
  }

  @Get(':id')
  async getUser(@Param('id') id: number): Promise<any> {
    const userDetail = await this.appService.getById(id);
    return userDetail;
  }
}

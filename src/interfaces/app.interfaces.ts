export interface User {
    id: number;
    name: string;
    email: string;
    password: string;
}

export interface CreateUserParams {
    name: string;
    email: string;
    password: string;   
}